import cv2
import numpy as np
from imutils import contours
from PIL import Image, ImageEnhance, ImageFilter
import pytesseract

pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'  # your path may be different




def get_contour_precedence(contour, cols):
    origin = cv2.boundingRect(contour)
    return origin[1] * cols + origin[0]

# Load image, grayscale, Otsu's threshold
image = cv2.imread('va.jpg')
array = []
original = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

# Remove text characters with morph open and contour filtering
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=1)
cnts = cv2.findContours(opening, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
for c in cnts:
    area = cv2.contourArea(c)
    if area < 500:
        cv2.drawContours(opening, [c], -1, (0,0,0), -1)

# Repair table lines, sort contours, and extract ROI
close = 255 - cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, iterations=1)
cnts = cv2.findContours(close, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
#max_height = np.max(c[::, 3])
#nearest = max_height * 1.4
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
#(cnts, _) = contours.sort_contours(cnts, method="top-to-bottom")

cnts.sort(key=lambda x:get_contour_precedence(x, image.shape[1]))
a = 0
for c in cnts:
    a = a + 1
    area = cv2.contourArea(c)
    if area < 25000:
        x,y,w,h = cv2.boundingRect(c)
        cv2.rectangle(image, (x, y), (x + w, y + h), (36,255,12), -1)
        ROI = original[y:y+h, x:x+w]
       
#        cv2.imwrite("Cropped.jpg", ROI)
#        im = Image.open("Cropped.jpg") # the second one 
#        im = im.filter(ImageFilter.MedianFilter())
#        enhancer = ImageEnhance.Contrast(im)
#        im = enhancer.enhance(2)
#        im = im.convert('1')
         #  TO GET IMAGESSSS   cv2.imwrite('salav' + str(a) + '.jpg', ROI)
#        text = pytesseract.image_to_string(Image.open('temp2.jpg'))
        

        text = pytesseract.image_to_string(ROI,  lang='digits1', config='--psm 10 --oem 3 -c tessedit_char_whitelist=0123456789')
        #text = pytesseract.image_to_string(ROI,config='-c tessedit_char_whitelist=0123456789 --psm 6', lang='eng')      
        #text = pytesseract.image_to_string(ROI,config='--psm 7 digits')  
        #text = pytesseract.image_to_string(ROI,lang='digits')    
        array.append(text)

        # Visualization
        #cv2.imshow('image', image)
        #cv2.imshow('ROI', ROI)
        #cv2.waitKey(20)

#cv2.imshow('opening', opening)
#cv2.imshow('close', close)
#cv2.imshow('image', image)
print(array)
cv2.waitKey()
